CC = avr-gcc
CFLAG = -mmcu=atmega8

AVRD = avrdude
AVRDFLAG = -c avrispmkII -p m8

avr.out: main.c
	${CC} ${CFLAG} -o avr.out main.c

clean:
	rm -f avr.out

upload:
	${AVRD} ${AVRDFLAG} -U flash:w:avr.out:e

