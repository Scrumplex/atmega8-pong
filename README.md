atmega8-pong
------------
PONG implemented on an ATmega 8, with a 12x10 LED matrix

# Details

This codebase has been created in conjunction with a school project utilizing an ATmega 8.

# Wiring

![Wiring Diagram](WiringDiagram.jpg)

# License

All of the work included in this repository is licensed under the GNU General Public License 3.0. Read the license text [here](LICENSE  )