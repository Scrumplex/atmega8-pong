/*
    ATmega8 PONG
    Copyright (C) 2020  Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define F_CPU 8000000 // needed for delay

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#define GAME_FPS 128
#define GAME_SPEED 4
#define DRAW_MS 2

#define X 12
#define Y 10

#define B_STROBE (1 << PB2)
#define B_CLK (1 << PB3)
#define B_DATA (1 << PB4)

#define D_PGND (1 << PD2)

#define C_Y ((1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3))
#define D_Y ((1 << PD4) | (1 << PD5) | (1 << PD6) | (1 << PD7))
#define B_Y ((1 << PB0) | (1 << PB1))

#define PONG_PLAYER_LENGTH 3
#define PONG_PLAYER_LEFT_X X - 1
#define PONG_PLAYER_RIGHT_X 0
#define PONG_BALL_INITIAL_X 5
#define PONG_BALL_INITIAL_Y 4
#define PONG_BALL_INITIAL_VX 1
#define PONG_BALL_INITIAL_VY 1

#define CH_MSK 0b111

struct game_state {
    uint8_t player_left_y;
    uint8_t player_right_y;
    uint8_t player_left_points;
    uint8_t player_right_points;

    uint8_t ball_x;
    uint8_t ball_y;
    int8_t ball_vector_x;
    int8_t ball_vector_y;
} game_state;

uint8_t random = 0;

uint8_t current_frame = 0;
uint8_t game_paused = 0;
struct game_state state = {};

double rand()
{
    random += 0b10011101;
    return random / 256.0;
}

int8_t rand_i(int8_t from, int8_t to)
{
    return rand() * (to - from) + from;
}

uint8_t rand_b()
{
    return rand() >= 0.5;
}

void sr_pulse()
{
    PORTB &= ~(B_CLK);
    PORTB |= B_CLK;
}

void sr_reset()
{
    PORTB |= B_STROBE;
    PORTB &= ~B_DATA;
    for (int i = 0; i < 12; i++) {
        sr_pulse();
    }
    // Cleanup
    PORTB &= ~B_STROBE;
}

uint32_t read_adc(uint8_t ch)
{
    ch &= CH_MSK;
    ADMUX = (ADMUX & ~CH_MSK) | ch; // clears the bottom 3 bits before ORing

    ADCSRA |= (1 << ADSC); // start ADC

    while (ADCSRA & (1 << ADSC)) // wait for result, TODO: use interrupts
        ;

    return (ADC);
}

void draw_line(uint16_t line)
{
    PORTB |= B_STROBE;
    for (int i = 0; i < X; i++) {
        if ((line >> i) & 1) {
            PORTB &= ~B_DATA;
        } else {
            PORTB |= B_DATA;
        }
        sr_pulse();
    }
    PORTB &= ~B_STROBE;
    PORTB &= ~B_DATA;
}

void draw_bitmap(uint16_t* bitmap)
{
    for (int i = 0; i < Y; i++) {
        draw_line(bitmap[i]);
        if (i < 4) {
            PORTC |= 1 << i;
        } else if (i < 8) {
            PORTD |= 1 << i;
        } else {
            PORTB |= 1 << (i - 8);
        }
        _delay_us(DRAW_MS * 1000 / Y);
        PORTC &= ~C_Y;
        PORTD &= ~D_Y;
        PORTB &= ~B_Y;
    }
}

int generate_bitmap(int16_t* bitmap, struct game_state state)
{
    for (int i = 0; i < state.player_left_points; i++) {
        bitmap[0] |= (1 << (X - 1 - i));
    }

    for (int i = 0; i < state.player_right_points; i++) {
        bitmap[0] |= (1 << i);
    }

    for (int i = 0; i < PONG_PLAYER_LENGTH; i++) {
        bitmap[i + state.player_left_y] |= (1 << 11);
        bitmap[i + state.player_right_y] |= (1 << 0);
    }

    bitmap[state.ball_y] |= (1 << state.ball_x);
}

void pause_game()
{
    game_paused = 1;
    DDRD &= ~D_PGND; // ground for potentiometers
    _delay_ms(1);
    GICR |= (1 << INT0);
}

void resume_game()
{
    GICR &= ~(1 << INT0);
    DDRD |= D_PGND; // ground for potentiometers
    PORTD &= ~D_PGND;
    game_paused = 0;
}

int game_reset(struct game_state* state)
{
    state->ball_x = PONG_BALL_INITIAL_X + rand_i(-2, 3);
    state->ball_y = PONG_BALL_INITIAL_Y + rand_i(-2, 3);
    state->ball_vector_x = PONG_BALL_INITIAL_VX * (rand_b() ? 1 : -1);
    state->ball_vector_y = PONG_BALL_INITIAL_VY * (rand_b() ? 1 : -1);
}

int game_tick()
{
    uint16_t bitmap[Y] = { 0 };

    if (current_frame >= GAME_FPS) {
        current_frame = 0;
    }

    // game logic
    if (!game_paused) {
        if (current_frame % (GAME_FPS / GAME_SPEED) == 0) { // move ball x times per second
            int8_t point_scored = 0;

            if (state.ball_y >= Y - 1 || state.ball_y <= 1) // 1 because the first row is for points
                state.ball_vector_y *= -1;

            if (state.ball_x >= PONG_PLAYER_LEFT_X - 1) { // is the ball at the left player
                if (state.ball_y <= state.player_left_y + PONG_PLAYER_LENGTH - 1 && state.ball_y >= state.player_left_y) // is it hitting the player
                    state.ball_vector_x *= -1;
                else {
                    state.player_right_points++;
                    point_scored = 1;
                }
            } else if (state.ball_x <= PONG_PLAYER_RIGHT_X + 1) { // is the ball at the right player
                if (state.ball_y <= state.player_right_y + PONG_PLAYER_LENGTH - 1 && state.ball_y >= state.player_right_y) // is it hitting the player
                    state.ball_vector_x *= -1;
                else {
                    state.player_left_points++;
                    point_scored = 1;
                }
            }
            if (point_scored) {
                if (state.player_left_points >= 5 || state.player_right_points >= 5) {
                    pause_game();
                } else {
                    game_reset(&state);
                }
            } else {
                state.ball_x += state.ball_vector_x;
                state.ball_y += state.ball_vector_y;
            }
        }
    }

    // build bitmap
    generate_bitmap(bitmap, state);

    // draw bitmap (takes around 2ms)
    draw_bitmap(bitmap);
    current_frame++;
}

void setup_io()
{
    // enable all rows for output
    DDRC |= C_Y;
    DDRD |= D_Y;
    DDRB |= B_Y;

    // enable control for shift registers
    DDRB |= B_STROBE | B_CLK | B_DATA;

    // reset all outputs
    PORTC = 0;
    PORTD = 0;
    PORTB = 0;
}

void setup_special()
{
    // setup TIMER
    ASSR |= (1 << AS2); // use external clock
    TCCR2 = (1 << CS20); // prescaler=0 32768HZ/256 = 128Hz
    TIMSK |= (1 << TOIE2); // enable interrupt

    // setup ADC
    ADMUX = 1 << REFS0;
    ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (0 << ADPS0); // prescaler = 64
    resume_game(); // also enabled ground for potentiometers

    sei();
}

int main(void)
{
    setup_io();
    setup_special();

    // reset shift relais
    sr_reset();
    // reset game
    game_reset(&state);

    while (1) {
        if (!game_paused) {
            state.player_right_y = floor(read_adc(7) / 1024.0 * 7) + 1;
            state.player_left_y = floor(read_adc(6) / 1024.0 * 7) + 1;
        }
    }
}

ISR(TIMER2_OVF_vect)
{
    game_tick();
}

ISR(INT0_vect)
{
    game_reset(&state);
    state.player_left_points = 0;
    state.player_right_points = 0;
    resume_game();
}